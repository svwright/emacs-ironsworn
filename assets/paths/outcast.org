*** Outcast

- [-] When your supply is reduced to 0, suffer any remaining -supply as -momentum. Then, roll +wits. On a strong hit, you manage to scrape by and take +1 supply. On a weak hit, you may suffer -2 momentum in exchange for +1 supply. On a miss, you are [[file:~/other/ironsworn/moves/suffer/out-of-supply.org][Out of Supply]].

- [ ] When you [[file:~/other/ironsworn/moves/relationship/sojourn.org][Sojourn]], you may reroll any dice. If you do (decide before your first roll), your needs are few, but your isolation sets you apart from others. A strong hit counts as a weak hit.

- [ ] When you [[file:~/other/ironsworn/moves/adventure/reach-your-destination.org][Reach Your Destination]] and score a strong hit, you recall or recognize something helpful about this place. Envision what it is, and take +2 momentum.

