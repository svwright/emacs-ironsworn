*** Animal Kin

- [-] When you make a move to pacify, calm, control, aid, or fend off an animal (or an animal or beast companion), add +1 and take +1 momentum on a hit.

- [ ] You may add or upgrade an animal or beast companion asset for 1 fewer experience. Once you mark all their abilities, you may [[file:~/other/ironsworn/moves/relationship/forge-a-bond.org][Forge a Bond]] with them and take an automatic strong hit. When you do, mark a bond twice and take 1 experience.

- [ ] Once per fight, when you leverage your animal or beast companion to make a move, reroll any dice. On a hit, take +1 momentum.

