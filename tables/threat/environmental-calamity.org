#+TITLE:  Threat: Environmental Calamity
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-27 December
#+TAGS:   rpg ironsworn

  - Devastate a place
  - Block a path
  - Threaten a community with imminent destruction
  - Manifest unexpected effects
  - Expand in scope or intensity
  - Allow someone to take advantage
  - Deprive of resources
  - Isolate an important person or community
  - Force refugees into hostile lands
  - Disrupt natural ecosystems
