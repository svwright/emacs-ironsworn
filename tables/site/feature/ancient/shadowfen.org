#+TITLE: Features in an Ancient Location


Roll on Table: d20
  |   1-4 | Evidence of lost knowledge            |
  |   5-8 | Inscrutable relics                    |
  |  9-12 | Ancient artistry or craft             |
  | 13-16 | Preserved corpses or fossils          |
  | 17-20 | Visions of this place in another time |
#+TITLE: Features in a Shadowfen

Roll on Table: d80 + 20
  | 21-43 | Narrow path through a fetid bog  |
  | 44-56 | Stagnant waterway                |
  | 57-64 | Flooded thicket                  |
  | 65-68 | Island of dry land               |
  | 69-72 | Submerged discovery              |
  | 73-76 | Preserved corpses                |
  | 77-80 | Overgrown structure              |
  | 81-84 | Tall reeds                       |
  | 85-88 | Camp or outpost                  |
  | 89-98 | Something unusual or unexpected  |
  |    99 | You transition into a new theme  |
  |   100 | You transition into a new domain |
