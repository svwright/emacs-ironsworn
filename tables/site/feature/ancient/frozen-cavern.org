#+TITLE: Features in an Ancient Location


Roll on Table: d20
  |   1-4 | Evidence of lost knowledge            |
  |   5-8 | Inscrutable relics                    |
  |  9-12 | Ancient artistry or craft             |
  | 13-16 | Preserved corpses or fossils          |
  | 17-20 | Visions of this place in another time |
#+TITLE: Features in a Frozen Cavern

Roll on Table: d80 + 20
  | 21-43 | Maze of icy tunnels              |
  | 44-56 | Glistening cave                  |
  | 57-64 | Vast chamber                     |
  | 65-68 | Frigid waterway                  |
  | 69-72 | Icy pools                        |
  | 73-76 | Magnificent ice formations       |
  | 77-80 | Frozen waterfall                 |
  | 81-84 | Deep crevasses                   |
  | 85-88 | Discovery locked in the ice      |
  | 89-98 | Something unusual or unexpected  |
  |    99 | You transition into a new theme  |
  |   100 | You transition into a new domain |
