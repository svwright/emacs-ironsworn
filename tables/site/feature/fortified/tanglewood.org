#+TITLE: Features in a Fortified Location

Roll on Table: d20
  |   1-4 | Camp or quarters                     |
  |   5-8 | Guarded location                     |
  |  9-12 | Storage or repository                |
  | 13-16 | Work or training area                |
  | 17-20 | Command center or leadership         |
#+TITLE: Features in a Tangled Wood

Roll on Table: d80 + 20
  | 21-43 | Dense thicket                    |
  | 44-56 | Overgrown path                   |
  | 57-64 | Waterway                         |
  | 65-68 | Clearing                         |
  | 69-72 | Elder tree                       |
  | 73-76 | Brambles                         |
  | 77-80 | Overgrown structure              |
  | 81-84 | Rocky outcrop                    |
  | 85-88 | Camp or outpost                  |
  | 89-98 | Something unusual or unexpected  |
  |    99 | You transition into a new theme  |
  |   100 | You transition into a new domain |
