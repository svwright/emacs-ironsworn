#+TITLE: Features in a Haunted Place

Roll on Table: d20
  |   1-4 | Tomb or burial site                   |
  |   5-8 | Blood was spilled here                |
  |  9-12 | Unnatural mists or darkness           |
  | 13-16 | Messages from beyond the grave        |
  | 17-20 | Apparitions of a person or event      |
#+TITLE: Features in a Shadowfen

Roll on Table: d80 + 20
  | 21-43 | Narrow path through a fetid bog  |
  | 44-56 | Stagnant waterway                |
  | 57-64 | Flooded thicket                  |
  | 65-68 | Island of dry land               |
  | 69-72 | Submerged discovery              |
  | 73-76 | Preserved corpses                |
  | 77-80 | Overgrown structure              |
  | 81-84 | Tall reeds                       |
  | 85-88 | Camp or outpost                  |
  | 89-98 | Something unusual or unexpected  |
  |    99 | You transition into a new theme  |
  |   100 | You transition into a new domain |
