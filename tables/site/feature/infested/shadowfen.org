#+TITLE: Features in an Infested Location

Roll on Table: d20
  |   1-4 | Inhabited nest                        |
  |   5-8 | Abandoned nest                        |
  |  9-12 | Ravaged terrain or architecture       |
  | 13-16 | Remains or carrion                    |
  | 17-20 | Hoarded food                          |
#+TITLE: Features in a Shadowfen

Roll on Table: d80 + 20
  | 21-43 | Narrow path through a fetid bog  |
  | 44-56 | Stagnant waterway                |
  | 57-64 | Flooded thicket                  |
  | 65-68 | Island of dry land               |
  | 69-72 | Submerged discovery              |
  | 73-76 | Preserved corpses                |
  | 77-80 | Overgrown structure              |
  | 81-84 | Tall reeds                       |
  | 85-88 | Camp or outpost                  |
  | 89-98 | Something unusual or unexpected  |
  |    99 | You transition into a new theme  |
  |   100 | You transition into a new domain |
