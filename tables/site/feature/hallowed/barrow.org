#+TITLE: Features in a Hallowed Location

Roll on Table: d20
  |   1-4 | Temple or altar              |
  |   5-8 | Offerings or atonements      |
  |  9-12 | Religious relic or idol      |
  | 13-16 | Consecrated ground           |
  | 17-20 | Dwellings or gathering place |
#+TITLE: Features of a Barrow

Roll on Table: d80 + 20
  | 21-43 | Burial chambers                  |
  | 44-56 | Maze of narrow passages          |
  | 57-64 | Shrine                           |
  | 65-68 | Stately vault                    |
  | 69-72 | Offerings to the dead            |
  | 73-76 | Statuary or tapestries           |
  | 77-80 | Remains of a grave robber        |
  | 81-84 | Mass grave                       |
  | 85-88 | Exhumed corpses                  |
  | 89-98 | Something unusual or unexpected  |
  |    99 | You transition into a new theme  |
  |   100 | You transition into a new domain |
