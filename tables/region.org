#+TITLE:  Ironsworn Oracle: Regions
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-08 December
#+TAGS:   rpg, ironsworn

Roll on Table: d100
  |  1-12 | Barrier Islands  |
  | 13-24 | Ragged Coast     |
  | 25-34 | Deep Wilds       |
  | 35-46 | Flooded Lands    |
  | 47-60 | Havens           |
  | 61-72 | Hinterlands      |
  | 73-84 | Tempest Hills    |
  | 85-94 | Veiled Mountains |
  | 95-99 | Shattered Wastes |
  |   100 | Elsewhere        |
