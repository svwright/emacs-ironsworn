#+TITLE:  Opportunities
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-27 December
#+TAGS:   rpg ironsworn

Roll on Table: d100
 |   1-25 | The terrain favors you, or you find a hidden path.              |
 |  26-45 | An aspect of the history or nature of this place is revealed.   |
 |  46-57 | You locate a secure area.                                       |
 |  58-68 | A clue offers insight or direction.                             |
 |  69-78 | You get the drop on a denizen.                                  |
 |  79-86 | This area provides an opportunity to scavenge, forage, or hunt. |
 |  87-90 | You locate an interesting or helpful object.                    |
 |  91-94 | You are alerted to a potential threat.                          |
 |  95-98 | You encounter a denizen who might support you.                  |
 | 99-100 | You encounter a denizen in need of help.                        |
