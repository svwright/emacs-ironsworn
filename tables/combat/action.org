#+TITLE:  Combat Actions
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-26 December
#+TAGS:   rpg ironsworn

Roll on Table: d100
 |   1-3 | Compel a surrender.                            |
 |   4-6 | Coordinate with allies.                        |
 |   7-9 | Gather reinforcements.                         |
 | 10-13 | Seize something or someone.                    |
 | 18-21 | Intimidate or frighten.                        |
 | 26-29 | Shift focus to someone or something else.      |
 | 34-39 | Take a decisive action.                        |
 | 46-52 | Ready an action.                               |
 | 61-68 | Leverage the advantage of a weapon or ability. |
 | 14-17 | Provoke a reckless response.                   |
 | 22-25 | Reveal a surprising truth.                     |
 | 30-33 | Destroy something, or render it useless.       |
 | 40-45 | Reinforce defenses.                            |
 | 53-60 | Use the terrain to gain advantage.             |
 | 69-78 | Create an opportunity.                         |
 | 79-89 | Attack with precision.                         |
 | 90-99 | Attack with power.                             |
 |   100 | Take a completely unexpected action.           |
