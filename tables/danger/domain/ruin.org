#+TITLE: Dangers found in Ruins

  - Ancient mechanism or trap
  - Collapsing wall or ceiling
  - Blocked or broken passage
  - Unstable floor above a new danger
  - Ancient secrets best left buried
