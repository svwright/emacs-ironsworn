#+TITLE:  Elf Names
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-26 December
#+TAGS:   rpg ironsworn

Ahmeshki
Anatu
Anunna
Arakhi
Aralu
Mayatanay
Arsula
Atani
Balathu
Belesunna
Cybela
Dismashk
Ditani
Dorosi
Dotani
Ereshki
Etana
Faraza
Gamanna
Gezera
Hullata
Ibrahem
Ilsit
Jemshida
Kendalanu
Kerihu
Kinzura
Leucia
Mattissa
Mitunu
Naidita
Nebakay
Nessana
Ninsunu
Otani
Retenay
Seleeku
Sidura
Sinosu
Sumula
Sutahe
Tahuta
Tishetu
Ukames
Uktannu
Uralar
Utamara
Vidarna
Visapni
Zursan
