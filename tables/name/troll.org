#+TITLE:  Troll Names
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-26 December
#+TAGS:   rpg ironsworn


Rattle
Scratch
Wallow
Groak
Gimble
Scar
Cratch
Creech
Shush
Glush
Slar
Gnash
Stoad
Grig
Bleat
Chortle
Cluck
Slith
Mongo
Creak
Burble
Vrusk
Snuffle
Leech
Herk
