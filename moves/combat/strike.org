** Strike

When you have initiative and attack in close quarters, roll +iron When you have initiative and attack at range, roll +edge.

On a *strong hit*, inflict +1 harm. You retain initiative.

On a *weak hit*, inflict your harm and lose initiative.

On a *miss*, your attack fails and you must [[file:../fate/pay-the-price.org][Pay the Price]]. Your foe has initiative.

*** Details
:PROPERTIES:
:move-stats: iron edge
:VISIBILITY: folded
:END:

Make this move when you have initiative and act to inflict harm on your foe. Narratively, this move might represent a focused moment in time—a single sweep of your axe or the flight of an arrow. Or, it can depict a flurry of attacks as you put your opponent on the defensive.

On a *strong hit*, you strike true. By default you inflict 2 harm if you are armed with a deadly weapon (such as a sword, axe, spear, or bow), and 1 harm if not. A *strong hit* on this move gives you an additional +1 harm (so, 3 harm with a deadly weapon). You may also have additional bonuses provided by assets.

Each point of harm you inflict is marked as progress on your foe’s progress track, as appropriate to their rank. For example, each point of harm equals 2 ticks when fighting an extreme enemy, or 2 full progress boxes when fighting a dangerous enemy.

Narratively, a *strong hit* represents wounding your enemy or wearing them down. You have initiative and can make your next move. If this attack was intended as a decisive blow, you can attempt to [[file:end-the-fight.org][End the Fight]].

On a *weak hit*, you’ve done some damage but have overextended or your foe counters. You mark your harm, and your foe has initiative. What do they do next?

On a *miss*, you must [[file:../fate/pay-the-price.org][Pay the Price]]. Your opponent strikes back and you [[file:../suffer/endure-harm.org][Endure Harm]]. You lose position or advantage and suffer -momentum. You face a new or intensified danger. A companion or ally is put in harm’s way. Your weapon is dropped or broken. Let the outcome flow out of the fiction, or roll on the [[file:../fate/pay-the-price.org][Pay the Price]] table to see what happens.

#+STARTUP: showall
# Local Variables:
# eval: (flycheck-mode -1)
# End:
