** Face Desolation

When you are brought to the brink of desolation, roll +heart.

On a *strong hit*, you resist and press on.

On a *weak hit*, choose one.
  - Your spirit or sanity breaks, but not before you make a noble sacrifice. Envision your final moments.
  - You see a vision of a dreaded event coming to pass. Envision that dark future ([[file:../fate/ask-the-oracle.org][Ask the Oracle]] if unsure), and [[file:../quest/swear-an-iron-vow.org][Swear an Iron Vow]] (formidable or extreme) to prevent it. If you fail to score a hit when you *Swear an Iron Vow*, or refuse the quest, you are lost. Otherwise, you return to your senses and are now tormented. You may only clear the tormented debility by completing the quest.

On a *miss*, you succumb to despair or horror and are lost.

*** Details
:PROPERTIES:
:move-stats: heart
:VISIBILITY: folded
:END:

You make this move when forced to *Face Desolation* as a result of a *miss* on the [[file:endure-stress.org][Endure Stress]] move. This represents the potential breaking point for your character. Do you push on in spite of all you have seen, all you have done, or do you fall into darkness?

Choosing the option to become tormented on a *weak hit* creates interesting story possibilities. What is your greatest fear? Preventing that dire outcome can steer your story in a compelling new direction.

On a *miss*, you are broken. There is no recovery possible. This is the end of your character’s story.

#+STARTUP: showall
# Local Variables:
# eval: (flycheck-mode -1)
# End:
